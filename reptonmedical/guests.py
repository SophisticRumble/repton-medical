import sys
import os
from django.core.wsgi import get_wsgi_application


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "reptonmedical.settings")

application = get_wsgi_application()

from django.conf import settings
from visitordatacollection.models import GuestProfile
from django.contrib.auth.models import User

def get_guest_info():

    guest_info_list = settings.BASE_DIR + "/guests.txt"

    my_file = open(guest_info_list, "r")

    _list = []

    guest_info_keys = ["first_name", "last_name", "email", "role", "phone"]

    for item in my_file:
        _split = item.split(',')

        _list.append(dict(zip(guest_info_keys, _split)))

    return _list

def set_guest_data():

    guest_list = get_guest_info()

    user = User.objects.get(id=1)

    for item in guest_list:
        GuestProfile.objects.create(first_name=item["first_name"], last_name=item["last_name"], email=item["email"], role=item["role"], phone=item["phone"], user=user)


set_guest_data()

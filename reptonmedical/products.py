import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "reptonmedical.settings")
from django.conf import settings


products_list = settings.BASE_DIR + "/products.txt"

my_file = open(products_list, "r")
products = []

for item in my_file:
    products.append(item.strip())

print(products)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from visitordatacollection.models import GuestProfile, Product

# Register your models here.

admin.site.register(GuestProfile)
admin.site.register(Product)

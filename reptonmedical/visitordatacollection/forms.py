from django.forms import ModelForm
from .models import GuestProfile, Product
from django import forms

class GuestProfileForm(ModelForm):

    class Meta:
        model = GuestProfile
        exclude = ['user', 'creation_date']


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['products'].queryset = Product.objects.all().order_by('name')

        for field in self.fields:
            self.fields[field].widget.attrs.update({
                'class': 'validate'
            })

    def save(self, commit=True):
        guest = super(GuestProfileForm, self).save(commit=False)
        if commit:
            guest.save()
            self.save_m2m()
        return guest

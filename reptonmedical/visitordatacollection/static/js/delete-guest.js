
$(document).ready(function() {


    $('.js-delete-guest').click(function() {
        var guest_id = $(this).data("id");

        var data = { "guest_id" : guest_id };


        $.ajax({
              type: "POST",
              url: delete_url,
              data: data
        }).done(function(data) {

            if (data['success']) // all keys set in python view can be accessed data[<key_name>]
            {
                $('#profile_' + guest_id).remove();
            }



        });

    });


});

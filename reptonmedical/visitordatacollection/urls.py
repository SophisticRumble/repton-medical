from django.urls import path

from . import views

urlpatterns = [
    path('', views.Login.as_view(), name="home"),
    path('login/', views.Login.as_view(), name='login'),
    path('guest/create/', views.CreateGuest.as_view(), name='create_guest'),
    path('guest/success/', views.GuestSuccess.as_view(), name='guest_success'),
    path('logout/', views.Logout.as_view(), name='logout'),
    path('guest/list/', views.GuestProfileList.as_view(), name='guest_list'),
    path('guestprofile/update/<int:pk>/', views.GuestProfileUpdate.as_view(), name='guest_update'),
    path('guestprofile/update_success/', views.GuestUpdateSuccess.as_view(), name='guest_update_success'),
    path('guestprofile/delete/<int:pk>/', views.GuestProfileDelete.as_view(), name='guest_delete'),
    path('guest/list/delete/', views.delete_guest, name="list_delete"),
]

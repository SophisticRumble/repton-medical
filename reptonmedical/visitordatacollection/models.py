# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse, reverse_lazy



# Create your models here.

class Product(models.Model):

    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class GuestProfile(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField(max_length=254, unique=True)
    role = models.CharField(max_length=255)
    phone = models.PositiveIntegerField()
    products = models.ManyToManyField(Product, blank=True)


    def __str__(self):
        return "user: {}, guest: {} {}".format(str(self.user), self.first_name, self.last_name)

    @property
    def full_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    def get_absolute_url(self):
        return reverse('guest_update', kwargs={'pk':self.pk})

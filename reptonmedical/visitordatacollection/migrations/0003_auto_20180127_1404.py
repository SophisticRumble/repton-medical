# Generated by Django 2.0.1 on 2018-01-27 14:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('visitordatacollection', '0002_auto_20180122_2104'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='guestprofile',
            name='products',
            field=models.ManyToManyField(blank=True, to='visitordatacollection.Product'),
        ),
    ]

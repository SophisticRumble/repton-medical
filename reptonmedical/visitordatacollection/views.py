# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, get_object_or_404, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic import CreateView, TemplateView, ListView, UpdateView, DeleteView, View
from django.views.generic.detail import BaseDetailView
from .forms import GuestProfileForm
from .models import GuestProfile
from django.urls import reverse, reverse_lazy, resolve
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import json


# Create your views here.

class Login(LoginView):

    template_name = 'login.html'
    redirect_field_name = reverse_lazy('guest_list')
    redirect_authenticated_user = reverse_lazy('guest_list')

    def login(request):

        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            print 'new line'
            return redirect('guest_list')

        return render(request, 'login_error.html')



class Logout(LogoutView):

    template_name = 'logout.html'

    def get_context_data(self, **kwargs):

    	context = super(Logout, self).get_context_data(**kwargs)
    	context['page'] = 'logout'
    	return context

    def logout_view(request):

        logout(request)
        return reverse_lazy('logout.html')



class CreateGuest(LoginRequiredMixin, CreateView):

    login_url = '/login/'
    redirect_field_name = 'guest_list'

    template_name = 'visitor_form.html'
    form_class = GuestProfileForm
    success_url = reverse_lazy('guest_success')


    def form_valid(self, form):

        guest_profile = form.save(commit=False)
        form.instance.user = self.request.user
        guest_profile.save()

        return super(CreateGuest, self).form_valid(form)



class GuestProfileList(ListView):

    template_name = 'guest_profile_list.html'
    model = GuestProfile
    paginate_by = 10


def delete_guest(request):

    guest_id = request.POST.get('guest_id', None)
    guest = GuestProfile.objects.filter(id=guest_id)

    data = {}
    data['success'] = False

    if guest:
        data['success'] = True
        guest.delete()

    return JsonResponse(data)



class GuestProfileUpdate(UpdateView):

    model = GuestProfile
    form_class = GuestProfileForm
    template_name = 'guestprofile_update_form.html'
    success_url = reverse_lazy('guest_update_success')


    def form_valid(self, form):

        guest_profile = form.save(commit=False)
        form.instance.user = self.request.user
        guest_profile.save()

        return super(GuestProfileUpdate, self).form_valid(form)


class GuestProfileDelete(DeleteView):

    model = GuestProfile
    success_url = reverse_lazy('guest_list')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)



class GuestSuccess(TemplateView):

    template_name = 'submission_success.html'



class GuestUpdateSuccess(TemplateView):

    template_name = 'guest_profile_update_success.html'

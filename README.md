"# My project's README" 

Work is done in a feature branch

# Update master to latest version
git checkout master 
git pull
# create a new feature branch
git checkout -b my_feature
Before the feature branch is merged into master it is rebased.
# Update master to latest version
git checkout master
git pull
# Rebase feature onto master branch
git checkout my_feature
git rebase master
At this point resolve any conflicts that occur (using git add [file] / git rebase --continue)
Then merge the feature into the master branch
git checkout master 
# Prefer to use no-ff to always have a merge commit. 
# (unless feature is a single commit then dont bother)
git merge my_feature --no-ff
